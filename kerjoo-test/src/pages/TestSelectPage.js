import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Form from 'react-bootstrap/Form';
import { Container } from 'react-bootstrap';
import {
    getProvinces,
    getRegencies,
    getDistricts,
    getVillages
} from '../redux/api'; 

import {
    selectProvince,
    selectRegency,
    selectDistrict,
    selectVillage
} from '../redux/selectReducer';

const TestSelectPage = () => {
    const dispatch = useDispatch();

    const select = useSelector((state) => state.select);
    const provinces = useMemo(() => select?.provinces || [], [select]);
    const regencies = useMemo(() => select?.regencies || [], [select]);
    const districts = useMemo(() => select?.districts || [], [select]);
    const villages = useMemo(() => select?.villages || [], [select]);

    useEffect(() => {
        dispatch(getProvinces());
    }, [dispatch]);

    const handleProvinceChange = (provinceId) => {
        dispatch(selectProvince(provinceId));
        dispatch(getRegencies(provinceId));
    };

    const handleRegencyChange = (regencyId) => {
        dispatch(selectRegency(regencyId));
        dispatch(getDistricts(regencyId));
    };

    const handleDistrictChange = (districtId) => {
        dispatch(selectDistrict(districtId));
        dispatch(getVillages(districtId));
    };

    const handleVillageChange = (villageId) => {
        dispatch(selectVillage(villageId));
    };

    return (
        <Container>
            <Form>
                <Form.Group controlId="provinceSelect">
                    <Form.Label>Provinsi:</Form.Label>
                    <Form.Control as="select" onChange={(e) => handleProvinceChange(e.target.value)}>
                        <option value="">Pilih Provinsi</option>
                        {provinces.map((province) => (
                            <option key={province.id} value={province.id}>
                                {province.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId="regencySelect">
                    <Form.Label>Kab/Kota:</Form.Label>
                    <Form.Control as="select" onChange={(e) => handleRegencyChange(e.target.value)}>
                        <option value="">Pilih Kab/Kota</option>
                        {regencies.map((regency) => (
                            <option key={regency.id} value={regency.id}>
                                {regency.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId="districtSelect">
                    <Form.Label>Kecamatan:</Form.Label>
                    <Form.Control as="select" onChange={(e) => handleDistrictChange(e.target.value)}>
                        <option value="">Pilih Kecamatan</option>
                        {districts.map((district) => (
                            <option key={district.id} value={district.id}>
                                {district.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId="villageSelect">
                    <Form.Label>Desa:</Form.Label>
                    <Form.Control as="select" onChange={(e) => handleVillageChange(e.target.value)}>
                        <option value="">Pilih Desa</option>
                        {villages.map((village) => (
                            <option key={village.id} value={village.id}>
                                {village.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Form>
        </Container>
    );
};

export default TestSelectPage;
