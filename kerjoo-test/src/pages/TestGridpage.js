import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const TestGridPage = () => {
    return (
        <Container>
            <Row>
                <Col className="border" lg={3} md={6}>Lorem Ipsum</Col>
                <Col className="border" lg={3} md={6}>Lorem Ipsum</Col>
                <Col className="border" lg={3} md={6}>Lorem Ipsum</Col>
                <Col className="border" lg={3} md={6}>Lorem Ipsum</Col>
            </Row>
        </Container>
    );
};

export default TestGridPage;
