// src/TestFlexPage.js
import React from 'react';
import { Container, Row, Col, Button, FormControl, Dropdown } from 'react-bootstrap';

const TestFlexPage = () => {
  return (
    <Container>
      <Row className="d-flex flex-warp">
        <Col xs={12} md={6} lg={4} className="mb-3 align-self-start">
          <Button className="w-100">Tambah</Button>
          <Button className="w-100 mt-2">Import</Button>
          <Button className="w-100 mt-2">Export</Button>
        </Col>

        <Col xs={12} md={6} lg={8} className="mb-3 align-self-start">
            <Row>
                <Col xs={12} md={12} lg={6} className="mb-3 align-self-start">
                    <FormControl placeholder="Search" />
                </Col>
            
                <Col xs={12} md={12} lg={6} className="mb-3 align-self-start">
                    <Dropdown>
                        <Dropdown.Toggle className="w-100" variant="primary">
                        Pilih Tahun
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                        <Dropdown.Item>2020</Dropdown.Item>
                        <Dropdown.Item>2021</Dropdown.Item>
                        <Dropdown.Item>2022</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Col>
            </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default TestFlexPage;
