import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import TestGridPage from './pages/TestGridpage';
import TestSelectPage from './pages/TestSelectPage';
import TestFlexPage from './pages/TestFlexPage';
import Navbar from './components/navbar';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Navigate to="/test-grid" />} />
        <Route path="/test-grid" element={<TestGridPage />} />
        <Route path="/test-flex" element={<TestFlexPage />} />
        <Route path="/test-select" element={<TestSelectPage />} />
      </Routes>
    </Router>
  );
}

export default App;
