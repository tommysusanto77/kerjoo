// src/redux/selectReducer.js
import { createSlice } from '@reduxjs/toolkit';

const selectSlice = createSlice({
  name: 'select',
  initialState: {
    provinces: [],
    regencies: [],
    districts: [],
    villages: [],
    selectedProvince: '',
    selectedRegency: '',
    selectedDistrict: '',
    selectedVillage: '',
  },
  reducers: {
    setProvinces: (state, action) => {
        state.provinces = action.payload;
    },
    setRegencies: (state, action) => {
      state.regencies = action.payload;
    },
    setDistricts: (state, action) => {
      state.districts = action.payload;
    },
    setVillages: (state, action) => {
      state.villages = action.payload;
    },
    selectProvince: (state, action) => {
      state.selectedProvince = action.payload;
    },
    selectRegency: (state, action) => {
      state.selectedRegency = action.payload;
    },
    selectDistrict: (state, action) => {
      state.selectedDistrict = action.payload;
    },
    selectVillage: (state, action) => {
      state.selectedVillage = action.payload;
    },
  },
});

export const {
  setProvinces,
  setRegencies,
  setDistricts,
  setVillages,
  selectProvince,
  selectRegency,
  selectDistrict,
  selectVillage,
} = selectSlice.actions;

export default selectSlice.reducer;
