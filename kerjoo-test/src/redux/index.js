// src/redux/index.js
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer'; // Import the combined reducers

const store = configureStore({
  reducer: rootReducer,
  // You can include middleware and other store configurations here
});

export default store;
