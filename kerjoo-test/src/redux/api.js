import { setProvinces, setRegencies, setDistricts, setVillages } from './selectReducer';
import axios from 'axios';

const API_BASE_URL = 'https://api.kerjoo.com/api/v1/reference';

export const getProvinces = () => async (dispatch) => {
  try {
    const response = await axios.get(`${API_BASE_URL}/provinces`);
    dispatch(setProvinces(response.data));
  } catch (error) {
    console.error('Error fetching provinces:', error);
  }
};

export const getRegencies = (provinceId) => async (dispatch) => {
  try {
    const response = await axios.get(`${API_BASE_URL}/regencies_of/${provinceId}`);
    dispatch(setRegencies(response.data));
  } catch (error) {
    console.error('Error fetching regencies:', error);
  }
};

export const getDistricts = (regencyId) => async (dispatch) => {
  try {
    const response = await axios.get(`${API_BASE_URL}/districts_of/${regencyId}`);
    dispatch(setDistricts(response.data));
  } catch (error) {
    console.error('Error fetching districts:', error);
  }
};

export const getVillages = (districtId) => async (dispatch) => {
  try {
    const response = await axios.get(`${API_BASE_URL}/villages_of/${districtId}`);
    dispatch(setVillages(response.data));
  } catch (error) {
    console.error('Error fetching villages:', error);
  }
};
