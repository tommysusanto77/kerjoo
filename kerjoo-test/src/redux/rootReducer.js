// src/redux/rootReducer.js
import { combineReducers } from 'redux';
import selectReducer from './selectReducer';

const rootReducer = combineReducers({
  select: selectReducer,
  // Add other reducers here if needed
});

export default rootReducer;
